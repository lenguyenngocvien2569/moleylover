export default function navigateTo(props, screen) {
    return props.navigation.navigate(screen);
}