import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AppNavigator from './src/navigations/navigator';
import { AppLoading } from 'expo';
export default function App() {
    const [isFontLoaded, setIsFontLoaded] = React.useState(false);

    return ( <
        AppNavigator / >
    );

}
////////////////////
let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});