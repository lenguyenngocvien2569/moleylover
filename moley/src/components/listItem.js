import React from 'react'
import {
    View,
    Text,
    Button,
} from "react-native";
import Total from './total'
import navigateTo from '../../utils/navigate'
import Item from './item'
export default function detail(props) {
    const handleButtonClick = () => {
        navigateTo(props,"Home");
    }
    return (
        <View>
            <Total></Total>
            <Item></Item>
        </View>
    )
}