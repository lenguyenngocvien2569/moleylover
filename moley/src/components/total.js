import React from 'react'
import {
    View,
    Text,
} from "react-native";
import style from '../styles/total.style';
export default function total() {
    return (
        <View style={style.container}>
            <View style={style.day}>
                <Text style={style.day_text}>22</Text>
            </View>
            <View style={style.date}>
                <Text>Thu nam</Text>
                <Text>thang 4 2021</Text>
            </View>
            <View style={style.total}>
                <Text style={style.total_text}> 8.000.000</Text>
            </View>
        </View>
    )
}