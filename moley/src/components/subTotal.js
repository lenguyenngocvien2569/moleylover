import React from 'react'
import {
    View,
    Text,
    Button,
} from "react-native";
import style from '../styles/subtotal.style'
export default function subTotal(props) {
    return (
        <View style={style.container}>
          <View style={style.element}>
              <Text style={{fontSize: 17}}>Tien vao</Text>
              <Text style={{fontSize: 17, color: '#0abf40'}}>9.000.000</Text>
          </View>
          <View style={style.element}>
              <Text style={{fontSize: 17}}>Tien ra</Text>
              <Text style={{fontSize: 17, color: '#e83849'}}>500.000</Text>
          </View>
          <View style={style.total}>
              <Text style={{fontSize: 18}}>Tong</Text>
          </View>
          <View style={style.button}>
            <Button title="xem bao cao"></Button>
          </View> 
        </View>
    )
}