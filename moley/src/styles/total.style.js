import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        height: 80,
        justifyContent: 'flex-start',
        display: 'flex',
        backgroundColor: '#aaa',
        marginTop: 15,
    },
    day: {
        width: '20%',
        alignItems: 'center'
    },
    day_text: {
        fontSize: 30
    },
    date: {
        width: '30%',
        alignItems: 'center'
    },
    date_text: {

    },
    total: {
        width: '50%',
        alignItems: 'center'
    },
    total_text: {
        fontSize: 20
    }
});