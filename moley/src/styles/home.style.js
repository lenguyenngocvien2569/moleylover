import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    nav_menu: {
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#fff",
        height: 70,
        position: 'absolute',
        bottom: 0,
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'space-around'
    },
    top_menu: {
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#fff",
        height: 70,
        top: 0,
        justifyContent: 'center',
    },
    mounth_menu: {
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#fff",
        height: 50,
        top: 0,
        justifyContent: 'space-around',
    },
    mounth_menu_text: {
        fontSize: 20,
    },
    mounth_menu_text_active: {
        fontSize: 20,
        backgroundColor: '#cff2bf',
    }
})