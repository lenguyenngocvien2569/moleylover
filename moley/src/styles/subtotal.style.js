import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: {
        width: '100%',
        height: 150,
        backgroundColor: '#fff'
    },
    element: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
    },
    total: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        right: 15,
        marginBottom: 15,
        marginTop: 5
    },
    button: {
        display: 'flex',
        justifyContent: 'center'
    }
});