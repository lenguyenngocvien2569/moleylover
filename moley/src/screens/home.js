import React from "react";
//import Icon from "@expo/vector-icons/MaterialCommunityIcons";
//import { ScrollView } from "react-native-gesture-handler";
import { Ionicons,FontAwesome5,AntDesign,Entypo,MaterialCommunityIcons   } from '@expo/vector-icons';
import { 
    TouchableHighlight,
    Text, 
    Modal, 
    ImageBackground,
    View,
    Image,
    Button,
    ScrollView,
} from "react-native";
import home_style from '../styles/home.style';
import navigateTo from "../../utils/navigate";
import background from '../../images/back.png';
import dots from '../../images/dots.png';
import filter from '../../images/filter.png';
import p from '../../images/p.png';
import {Header,Avatar,Tab,Icon} from 'react-native-elements'
import ListItem from '../components/listItem'
import SubTotal from '../components/subTotal';
export default function home(props){
    const [modalVisible, setmodalVisible] = React.useState(false);
    const handleButtonClick = () => {
        navigateTo(props,"Detail");
    }
    const setModalVisible = (visible) => {
        setmodalVisible(visible);
    }
    return (
        <ImageBackground
            source={background}
            style={{width:"100%",height:"100%"}}>
            <Header
                placement="left"
                leftComponent={
                    <Icon
                        name='sc-telegram'
                        type='evilicon'
                        color="#12de0b"
                    />
                }
                centerComponent={{ text: '9.000.000', style: { color: '#fff' } }}
                rightComponent={{ icon: 'home', color: '#fff' }}
            />
            {/* <View style={home_style.top_menu}>
                <FontAwesome5 
                    name="book" 
                    size={30} 
                    color="#a2a2db"
                    style={{width:20}}
                />
            </View> */}
            <View style={home_style.mounth_menu}>
                <ScrollView contentContainerStyle={{flexDirection:"row",justifyContent:"space-around"}} >                 
                    <View>
                        <Text style={home_style.mounth_menu_text}>Tháng trước</Text>
                    </View>
                    <View>
                        <Text style={home_style.mounth_menu_text}>Tháng này</Text>
                    </View>
                    <View>
                        <Text style={home_style.mounth_menu_text}>Tương lai</Text>
                    </View>
                </ScrollView>
            </View>

            <SubTotal></SubTotal>
            <View>
                <ScrollView style={{marginBottom: 100}}>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                    <ListItem></ListItem>
                </ScrollView>
            </View>
            
            
            <View style={home_style.nav_menu}>
                <FontAwesome5 
                    name="book" 
                    size={30} 
                    color="#a2a2db"
                    style={{width:20}}
                />
                <AntDesign  
                    name="dashboard" 
                    size={30} 
                    color="#a2a2db"
                    style={{width:20}}
                />
                <Ionicons 
                    onPress={handleButtonClick}
                    name="add-circle" 
                    size={55} 
                    color="#12de0b"
                    style={{width:20}}
                />
                <Entypo  
                    name="clipboard" 
                    size={30} 
                    color="#a2a2db"
                    style={{width:20}}
                />
                <MaterialCommunityIcons  
                    name="clipboard-account" 
                    size={30} 
                    color="#a2a2db"
                    style={{width:20}}
                />
            </View>

        </ImageBackground>    
    )
}