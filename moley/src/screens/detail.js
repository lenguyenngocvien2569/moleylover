import React from 'react'
import {
    Text,
    Button,
} from "react-native";
import navigateTo from '../../utils/navigate'
export default function detail(props) {
    const handleButtonClick = () => {
        navigateTo(props,"Home");
    }
    return (
        <Button onPress={handleButtonClick} title='Go to home'></Button>
    )
}