import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import home from '../screens/home';
import detail from '../screens/detail';
const stackNavigatorOptions = {
    headerShown: false
}
const AppNavigator = createStackNavigator({
    Home: { screen: home },
    Detail: { screen: detail },
}, {
    defaultNavigationOptions: stackNavigatorOptions
});

export default createAppContainer(AppNavigator)