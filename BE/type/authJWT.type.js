const { jwtConfig } = require('../config/index')
module.exports = {
    accessToken: {
        key: 'access_token',
        secret: jwtConfig.accessTokenConfig.secret
    },
    refreshToken: {
        key: 'refresh_token',
        secret: jwtConfig.refreshTokenConfig.secret
    }
}