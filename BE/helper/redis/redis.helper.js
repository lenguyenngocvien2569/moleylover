const redis = require('redis')
const redisConfig = require('../../config/index')
const client = redis.createClient({
    port: redisConfig.redisConfig.port,
    host: redisConfig.redisConfig.host
})
module.exports = { client }