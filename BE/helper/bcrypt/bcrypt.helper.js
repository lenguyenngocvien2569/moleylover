const bcrypt = require('bcrypt');
const { bcryptConfig } = require('../../config/index')

const bcryptHash = (plainText) => {
    return bcrypt.hash(plainText, parseInt(bcryptConfig.bcryptConfig.saltRounds))
}
const bcryptCompare = (plainText, hashText) => {
    return bcrypt.compare(plainText, hashText)
}

module.exports = {
    bcryptCompare,
    bcryptHash
}