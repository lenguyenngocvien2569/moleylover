class User {
    constructor(email, password) {
        this.email = email;
        this.password = password;
    }
}
const handlePaginationSuccess = (data, pagination, res) => {
    res.json({
        status: "success",
        data: data,
        pagination: pagination
    })
}
const handleSuccess = (data, res) => {
    res.json({
        status: "success",
        data: data
    })
}
module.exports = {
    User,
    handleSuccess,
    handlePaginationSuccess
}