const { SIGNAL_ERROR_CODE } = require('./signal_error_code');
const { requestStatusErr } = require('./error_code')

class CustomError extends Error {
    constructor(status, code, message) {
        super()
        this.status = status;
        this.code = code;
        this.message = message;
    }
}
class ErrorHandler extends Error {
    constructor(errorCode) {
        super()
        this.status = "Error !";
        this.code = errorCode.code;
        this.message = errorCode.message
    }
}
const handleErr = (err, res) => {
    res.json({
        status: "Error",
        code: err.code,
        message: err.message
    })
}


const handleRequestErr = (err, res) => {
    res.json({
        status: "Error",
        code: requestStatusErr[err].code,
        message: requestStatusErr[err].message
    })

}
const handleError = (reqErr, res) => {
    const err = parseSQLExceptionIfNeeded(reqErr)
    let { message } = err
    const { code, statusCode } = err

    res.status(statusCode || SIGNAL_ERROR_CODE.badRequest.code).send({
        status: 'Error',
        code,
        message
    })
}

const parseSQLExceptionIfNeeded = (err) => {
    const { code, sqlMessage } = err
    if (code !== "ER_SIGNAL_EXCEPTION") {
        return err
    }
    if (sqlMessage in SIGNAL_ERROR_CODE) {
        return new ErrorHandler(SIGNAL_ERROR_CODE[sqlMessage])
    }
    return new ErrorHandler(SIGNAL_ERROR_CODE.sensitive)
}

module.exports = {
    CustomError,
    ErrorHandler,
    handleErr,
    handleError,
    handleRequestErr
}