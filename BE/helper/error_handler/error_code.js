const requestStatusErr = {
    badRequest: {
        code: 400,
        message: "Bad request !"
    },
    unauthorized: {
        code: 401,
        message: "Unauthorized !"
    },
    forbidden: {
        code: 403,
        message: "Forbidden !"
    },
    notFound: {
        code: 404,
        message: "Not found !"
    }
}
const VALIDATION_ERROR = {
    VALIDATION_FAIL: {
        code: 400,
        message: "Validation fail !"
    }
}
const JWT_ERROR = {
    verifyToken: {
        code: 401,
        message: "Verify token error !"
    },
    blackList: {
        code: 401,
        message: "Token is available in blacklist !"
    },
    addToBlackList: {
        code: 401,
        message: "Add token to blacklist fail !"
    },
    decodedFail: {
        code: 401,
        message: "Decoded fail !"
    },
    invalidRefreshToken: {
        code: 401,
        message: "Refresh token invaliad !"
    }
}
const MULTER_ERR = {
    multerError: {
        code: 400,
        message: "Multer error !"
    }
}

const sqlStatusErr = [
    ER_EMPTY_QUERY = {
        code: 'ER_EMPTY_QUERY',
        message: "SQL NGU"
    },
    ER_NGU = {
        message: "ss"
    }
]

module.exports = {
    requestStatusErr,
    sqlStatusErr,
    JWT_ERROR,
    VALIDATION_ERROR,
    MULTER_ERR
}