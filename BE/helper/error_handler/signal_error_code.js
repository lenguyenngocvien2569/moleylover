const SIGNAL_ERROR_CODE = {
    badRequest: {
        code: 400,
        message: "bad request !"
    },
    sensitive: {
        code: 401,
        message: "sensitive !"
    },
    email_exists: {
        code: 401,
        message: "Email exits !"
    },
    userName_exists: {
        code: 401,
        message: "UserName exists !",
    },
    user_join_event: {
        code: 401,
        message: "Event has expired !"
    },
    user_joined_event: {
        code: 401,
        message: "User joined event !"
    },
    event_period: {
        code: 401,
        message: "Not included in the event period !"
    },
    email_not_exists: {
        code: 401,
        message: "Email not exists !"
    },
    event_track: {
        code: 401,
        message: "Event track fail !"
    }
}

module.exports = {
    SIGNAL_ERROR_CODE,
}