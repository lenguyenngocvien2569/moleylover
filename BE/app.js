const express = require('express');
const { handleError } = require('./helper/error_handler/err_handler');
const app = express()
require('dotenv').config();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static('public'));

app.get('/test', (req, res) => {
    res.json('test');
})

const service_module = require('./module/service/service.route');
const user_module = require('./module/user/user.route')
const wallet_module = require('./module/wallet/wallet.route')
const auth = require('./module/auth/auth.middleware');
app.use('/service', auth.verifyAccessToken, service_module);
app.use('/user', user_module);
app.use('/wallet', auth.verifyAccessToken, wallet_module);
app.use((err, req, res, next) => {
    handleError(err, res);
})

app.listen(process.env.PORT, () => {
    console.log(`Server listening on port ${process.env.PORT}`);
})