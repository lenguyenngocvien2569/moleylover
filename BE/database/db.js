const mysql = require('mysql')
const dbConfig = require('./db.config')
const pool = mysql.createPool(dbConfig)

const query = async(rawSql, params) => new Promise((resolve, reject) => {
    pool.query(rawSql, params, (error, results) => {
        if (error) reject(error)
        resolve(results)
    })
})

const sproc = async(procName, params) => new Promise((resolve, reject) => {
    const query = `CALL ${procName}(${escapeParams(params)})`
    pool.query(query, (error, results) => {
        if (error) reject(error)
        resolve(results)
    })
})

const escapeParams = (params) => {
    let escapeParams = []
    for (let param of params) {
        escapeParams.push(mysql.escape(param))
    }
    return escapeParams.join(',')
}
module.exports = {
    pool,
    query,
    sproc
}