const { bcryptCompare, bcryptHash } = require('../../helper/bcrypt/bcrypt.helper')
const { generatorToken, revokeToken } = require('../auth/auth.middleware')
const { register, login } = require('./user.model')
const authJwtType = require("../../type/authJwt.type")
const date = require('date-and-time');
const { dateTimeConfig } = require('../../config');
const { handleSuccess } = require('../../helper/response_handler/user_success_response');
const { JWT_ERROR } = require('../../helper/error_handler/error_code');
module.exports = {
    register: async(req, res, next) => {
        try {
            const { userName, password } = req.body;
            if (userName && password) {
                const hashPassword = await bcryptHash(password);
                const newUser = await register(userName, hashPassword, new Date(), new Date())
                if (newUser[0].status) {
                    handleSuccess(newUser, res);
                } else {
                    const user = {};
                    user.id_user = newUser[0].id_user;
                    user.role_id = newUser[0].role_id;
                    const gennerate = await generatorToken(user);
                    handleSuccess(gennerate, res);
                }
            }
        } catch (error) {
            next(error)
        }
    },
    login: async(req, res, next) => {
        try {
            const { userName, password } = req.body;
            if (userName && password) {
                const user = await login(userName);
                if (user[0].status) {
                    handleSuccess(user[0], res)
                } else {
                    let tempUser = {};
                    tempUser.id_user = user[0].id_user;
                    tempUser.role_id = user[0].role_id;
                    tempUser.userName = user[0].userName;
                    const comparePassword = await bcryptCompare(password, user[0].password);
                    if (comparePassword === true) {
                        console.log(1)
                        const gennerate = await generatorToken(tempUser);
                        console.log(gennerate)
                        handleSuccess(gennerate, res);
                    }
                }
            }
        } catch (error) {
            next(error)
        }
    },
}