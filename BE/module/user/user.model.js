const { sproc } = require("../../database/db");
const register_ML = `register_ML`;
const login_ML = `login_ML`;
module.exports = {
    register: async(userName, hashPassword, reg_date, update_date) => {
        const result = await sproc(register_ML, [userName, hashPassword, reg_date, update_date]);
        return result[0];
    },
    login: async(userName) => {
        const result = await sproc(login_ML, [userName]);
        return result[0];
    }
}