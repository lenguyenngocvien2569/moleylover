const { handleSuccess } = require('../../helper/response_handler/user_success_response');
const { userSpend, createWallet, updateWallet, deleteWallet, getDetailWallet, getAllWallet, getSpend } = require('./wallet.model')
module.exports = {
    get: async(req, res, next) => {
        try {
            const wallets = await getAllWallet(req.userId);
            handleSuccess(wallets[0], res);
        } catch (error) {
            next(error);
        }
    },
    create: async(req, res, next) => {
        try {
            const { walletName, amount } = req.body;
            const newWallet = await createWallet(walletName, new Date(), new Date(), req.userId, amount);
            handleSuccess(newWallet[0], res);
        } catch (error) {
            next(error);
        }
    },
    delete: async(req, res, next) => {
        try {
            const { wallet_id } = req.body;
            const deleted = await deleteWallet(wallet_id);
            handleSuccess(deleted, res)
        } catch (error) {
            next(error);
        }
    },
    detail: async(req, res, next) => {
        try {
            const { wallet_id } = req.body;
            const wallet = await getDetailWallet(wallet_id);
            handleSuccess(wallet[0], res)
        } catch (error) {
            next(error);
        }
    },
    update: async(req, res, next) => {
        try {
            const { wallet_id, wallet_name, amount } = req.body;
            const updated = await updateWallet(wallet_id, wallet_name, new Date(), amount);
            handleSuccess(updated, res);
        } catch (error) {
            next(error);
        }
    },
    spend: async(req, res, next) => {
        try {
            const { walletId, spend, service_id } = req.body;
            const walletSpend = await userSpend(req.userId, walletId, spend, new Date(), service_id);
            handleSuccess(walletSpend[0], res);
        } catch (error) {
            next(error)
        }

    },
    getSpend: async(req, res, next) => {
        try {
            const result = await getSpend(req.userId, req.params.month);
            let total = 0;
            result[0].forEach(element => {
                total = total + element.spend;
            });
            handleSuccess({ total: total, detail: result[0] }, res);
        } catch (error) {
            next(error)
        }
    }
}