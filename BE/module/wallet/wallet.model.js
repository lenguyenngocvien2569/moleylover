const { sproc } = require("../../database/db");
const wallet_sproc_get_all = `wallet_sproc_get_all`;
const wallet_sproc_get_detail = `wallet_sproc_get_detail`;
const wallet_sproc_create = `wallet_sproc_create`;
const wallet_sproc_update = `wallet_sproc_update`;
const wallet_sproc_delete = `wallet_sproc_delete`;
const wallet_spend = `wallet_spend`;
const wallet_get_spend = `wallet_get_spend`;
module.exports = {
    getAllWallet: async(user_id) => {
        const result = await sproc(wallet_sproc_get_all, [user_id]);
        return result;
    },
    getDetailWallet: async(id) => {
        const result = await sproc(wallet_sproc_get_detail, [id]);
        return result;
    },
    createWallet: async(walletName, created_at, updated_at, user_id, amount) => {
        const result = await sproc(wallet_sproc_create, [walletName, created_at, updated_at, user_id, amount]);
        return result;
    },
    updateWallet: async(wallet_id, wallet_name, updated_at, amount) => {
        const result = await sproc(wallet_sproc_update, [wallet_id, wallet_name, updated_at, amount]);
        return result;
    },
    deleteWallet: async(id) => {
        const result = await sproc(wallet_sproc_delete, [id]);
        return result;
    },
    userSpend: async(user_id, wallet_id, spend, created_at, service_id) => {
        const result = await sproc(wallet_spend, [user_id, wallet_id, spend, created_at, service_id])
        return result;
    },
    getSpend: async(user_id, month) => {
        const reuslt = await sproc(wallet_get_spend, [user_id, month]);
        return reuslt;
    }
}