const express = require('express')
const router = express.Router()
const controller = require('./wallet.controller');

router.post('/create', controller.create);
router.post('/delete', controller.delete);
router.post('/update', controller.update);
router.get('/detail', controller.detail);
router.get('/get', controller.get);
router.post('/spend', controller.spend);
router.get('/spend/get/:month', controller.getSpend);

module.exports = router;