const config = require('../../config/index');
const { client } = require('../../helper/redis/redis.helper');
const { ErrorHandler } = require('../../helper/error_handler/err_handler');
const { requestStatusErr, JWT_ERROR } = require('../../helper/error_handler/error_code');
const jwt = require('jsonwebtoken');
const authJwtType = require('../../type/authJWT.type');

const getTokenFromHeader = (req) => {
    if (!req.headers.authorization) {
        throw new ErrorHandler(requestStatusErr.unauthorized)
    }
    const token = req.headers.authorization
    return token
}

//Decode token
const decodeToken = (token) => jwt.decode(token)

//Get token from store
const getStoredToken = (key, userId) => {
        return new Promise((resolve, reject) => {
            client.hget(key, userId, (err, tokens) => {
                if (err) {
                    return reject(err)
                }
                resolve(tokens === null ? new Set() : new Set(tokens.split(',')))
            })
        })
    }
    //isAdmin 
const isAdmin = (req, res, next) => {
        if (req.userRole === 2) {
            next()
        } else {
            throw new Error("User is not admin !")
        }
    }
    //isUser 
const isUser = (req, res, next) => {
    if (req.userRole === 1) {
        next()
    } else {
        throw new Error("This is not user !")
    }
}

//Check valid token 
const validateToken = (token, key) => {
    return new Promise((resolve, reject) => {
        const decodedToken = jwt.decode(token)
        const userId = decodedToken && decodedToken.id_user
        if (!userId) {
            throw new ErrorHandler(JWT_ERROR.decodedFail)
        }
        client.hexists(key, userId, async(err, reply) => {
            if (reply === 1) {
                const currentToken = await getStoredToken(key, userId)
                return resolve(currentToken.has(token))
            }
            reject(new ErrorHandler(JWT_ERROR.blackList))
        })
    })
}

const verifyAccessToken = async(req, res, next) => {
    try {
        const token = getTokenFromHeader(req)
        const isValid = await validateToken(token, authJwtType.accessToken.key)
        if (!token || !isValid) {
            throw new ErrorHandler(requestStatusErr.unauthorized)
        }
        const decodedToken = jwt.verify(token, authJwtType.accessToken.secret)
        if (decodedToken) {
            req.userId = decodedToken.id_user
            req.userRole = decodedToken.role_id
            req.userName = decodedToken.userName
            return next()
        }
        throw new ErrorHandler(requestStatusErr.unauthorized)
    } catch (error) {
        next(error)
    }
}
const sign = (user, secret, expireTime) => {
    const payload = {
        id_user: user.id_user,
        role_id: user.role_id,
        userName: user.userName,
    }
    const token = jwt.sign(payload, secret, {
        expiresIn: expireTime
    })
    return token
}
const storeToken = async(token, key) => {
    const decodedToken = decodeToken(token)
    const userId = decodedToken && decodedToken.id_user
    if (!userId) throw new ErrorHandler(JWT_ERROR.decodedFail)
    const currentTokens = await getStoredToken(key, userId)
    currentTokens.add(token)
    client.hset(key, userId, [...currentTokens].join(','))
}
const generatorToken = async(user) => {
    const token = await sign(user, config.jwtConfig.accessTokenConfig.secret, config.jwtConfig.accessTokenConfig.expiresTime)
    const refreshToken = await sign(user, config.jwtConfig.refreshTokenConfig.secret, config.jwtConfig.refreshTokenConfig.expiresTime)
    await storeToken(token, authJwtType.accessToken.key)
    await storeToken(refreshToken, authJwtType.refreshToken.key)
    const a = await decodeToken(token)
    console.log(a)
    return { token, refreshToken }
}
const revokeToken = async(token, key) => {
    const decodedToken = decodeToken(token)
    const userId = decodedToken && decodedToken.id_user
    const currentTokens = await getStoredToken(key, userId)
    currentTokens.delete(token)
    client.hset(key, userId, [...currentTokens].join(','))
}
const checkAccessToken = async(req, res, next) => {
    try {
        const token = getTokenFromHeader(req)
        const decodedToken = jwt.verify(token, authJwtType.accessToken.secret)
        req.userId = decodedToken.id_user
        req.userRole = decodedToken.role_id
        req.userName = decodeToken.userName
        req.accessToken = token
        next()
    } catch (error) {
        next()
    }
}
const verifyRefreshToken = async(req, res, next) => {
    try {
        const { refreshToken } = req.body
        const isValid = await validateToken(refreshToken, authJwtType.refreshToken.key)
        if (!refreshToken || !isValid) {
            throw new ErrorHandler(JWT_ERROR.invalidRefreshToken)
        }
        const decodedToken = jwt.verify(refreshToken, authJwtType.refreshToken.secret)
        if (decodeToken) {
            req.userId = decodedToken.id_user
            req.userRole = decodedToken.role_id
            req.userName = decodedToken.userName
            return next()
        }
        throw new ErrorHandler(JWT_ERROR.decodedFail)
    } catch (error) {
        next(error)
    }
}
module.exports = {
    generatorToken,
    verifyAccessToken,
    revokeToken,
    checkAccessToken,
    verifyRefreshToken,
    isAdmin,
    isUser
}