const { sproc } = require("../../database/db");
const service_proc_get_all = `service_proc_get_all`;
const get_service_category = `get_service_category`;
const service_by_category = `service_by_category`;
module.exports = {
    // getAllService: async(limit, offset) => {
    //     return await sproc(service_proc_get_all, [limit, offset])[0];
    // }
    getAllService: async() => {
        const result = await sproc(service_proc_get_all, []);
        return result;
    },
    getCategory: async() => {
        const result = await sproc(get_service_category, []);
        return result;
    },
    getServiceByCategory: async(id) => {
        const result = await sproc(service_by_category, [id])
        return result
    }
}