const { getAllService, getCategory, getServiceByCategory } = require('./service.model')
module.exports = {
    getAllService: async(req, res, next) => {
        try {
            // let { page, perPage } = req.query
            // if (page <= 0) page = 1
            // if (perPage <= 0) perPage = 1
            // const events = await getAllService(perPage, (page - 1) * perPage)
            const events = await getAllService();
            console.log(events)
            res.json(events)
        } catch (error) {
            next(error)
        }
    },
    getServiceByCategory: async(req, res, next) => {
        try {
            const result = await getServiceByCategory(req.params.id)
            res.json(result)
        } catch (error) {
            next(error)
        }
    },
    getCategory: async(req, res, next) => {
        try {
            const category = await getCategory();
            res.json(category)
        } catch (error) {
            next(error)
        }
    }
}