const express = require('express')
const router = express.Router()
const controller = require('./service.controller')

router.get('/get', controller.getAllService);
router.get('/category', controller.getCategory);
router.get("/get/:id", controller.getServiceByCategory)
module.exports = router;