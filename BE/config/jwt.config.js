const accessTokenConfig = {
    secret: process.env.SECRET,
    expiresTime: process.env.TOKEN_TIME_OUT
}
const refreshTokenConfig = {
    secret: process.env.REFRESH_TOKEN_SECRET,
    expiresTime: process.env.REFRESH_TOKEN_TIME_OUT
}

module.exports = {
    accessTokenConfig,
    refreshTokenConfig
}