const bcryptConfig = {
    saltRounds: process.env.BCRYPT_SALT_ROUND
}
module.exports = { bcryptConfig }