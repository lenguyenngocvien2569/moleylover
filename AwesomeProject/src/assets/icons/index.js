import React from 'react';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export const IconBar = (props) => <FontAwesome5 name="bars" {...props} />;
export const IconCog = (props) => <FontAwesome5 name="cog" {...props} />;
export const IconSignOut = (props) => <FontAwesome5 name="sign-out-alt" {...props} />;
export const IconTachometer = (props) => <FontAwesome5 name="tachometer-alt" {...props} />;
export const IconArrowLeft = (props) => <FontAwesome5 name="arrow-left" {...props} />;
export const IconAddressBook = (props) => <FontAwesome5 name="address-book" {...props} />;
export const IconGoogle = (props) => <FontAwesome5 name="google" {...props} />;
export const UserCircle = (props) => <FontAwesome5 name="user-circle" {...props} />;
export const Lock = (props) => <FontAwesome5 name="lock" {...props} />;
export const ShowPass = (props) => <FontAwesome5 name="eye" {...props} />;
export const HidePass = (props) => <FontAwesome5 name="eye-slash" {...props} />;
export const IconQuestion = (props) => <FontAwesome5 name="question" {...props} />;
export const IconMenu = (props) => <FontAwesome5 name="ellipsis-v" {...props} />;
export const IconDelete = (props) => <FontAwesome5 name="trash-alt" {...props} />;
export const IconUpdate = (props) => <FontAwesome5 name="pencil-alt" {...props} />;
export const IconInfo = (props) => <FontAwesome5 name="file-alt" {...props} />;