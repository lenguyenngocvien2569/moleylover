
export const textColors = {
    white: '#ffffff',
    light: '#666666',
    primary: '#494949',
    info: '#267aff',
    success: '#4ca750',
    warn: '#fea220',
    error: '#e94440',
    danger: '#e94440',
    placeholder: 'rgba(73,73,73,0.5)',
    note: '#a540b8',
};


export const colors = {
    none: 'transparent',

    bgColor: '#f3f2f2',
    bgDefault: 'rgba(73, 73, 73, 0.08)',
    bgButton: 'rgba(73, 73, 73, 0.1)',
    bgDefaultHover: 'rgba(73, 73, 73, 0.2)',

    defaultHover: 'rgba(85, 89, 109, 0.1)',

    white: '#ffffff',
    black: '#000000',

    base: '#7d7d7d',

    primary: '#494949',
    primaryLight: '#666666',
    primaryDim: '#808080',

    gray: '#cccccc',
    grayLight: '#eaeaea',
    grayDark: '#585d71',
    grayDim: '#9C9C9C',

    blue: '#267aff',
    blueLight: '#57bfdb',
    blueLess: '#848c99',
    blueDim: '#616E82',
    blueGray: '#4a5b75',
    blueDark: '#404b5c',

    green: '#4ca750',
    greenLight: '#32c991',

    red: '#e94440',
    redLight: '#fb7b65',

    orange: '#ffa850',
    orangeLight: '#e9bc5a',
    orangeDark: '#fea220',

    purple: '#a540b8',

    call_in: '#267aff',
    call_out: '#4ca750',
    call_answer: '#fea220',
    call_no_answer: '#e94440',
    call_local: '#a540b8',
    call_unknow: '#7f8c8d',

};


export const boxShadows = {
    base: 'rgba(125, 125, 125, 0.08)',
    baseHover: 'rgba(125, 125, 125, 0.2)',
    baseDark: 'rgba(125, 125, 125, 0.5)',
    baseBlack: 'rgba(125, 125, 125, 0.8)',
    baseLight: 'rgba(73, 73, 73, 0.16)',

    primary: 'rgba(73, 73, 73, 0.08)',
    primaryHover: 'rgba(73, 73, 73, 0.2)',
    primaryDark: 'rgba(73, 73, 73, 0.5)',
    primaryBlack: 'rgba(73, 73, 73, 0.8)',

    grayLight: 'rgb(233, 233, 233, 0.5)',
    grayLightHover: 'rgb(233, 233, 233,0.8)',
    grayDark: 'rgba(85, 89, 109, 0.5)',
    grayDarkHover: 'rgba(85, 89, 109, 0.8)',

    blue: 'rgba(38, 122, 255, 0.5)',
    blueHover: 'rgba(38, 122, 255, 0.8)',
    blueLight: 'rgba(87, 191, 219, 0.5)',
    blueLightHover: 'rgba(87, 191, 219, 0.8)',

    green: 'rgba(76, 167, 80, 0.5)',
    greenHover: 'rgba(76, 167, 80, 0.8)',

    orange: 'rgba(254, 162, 32, 0.5)',
    orangeHover: 'rgba(254, 162, 32, 0.8)',
    orangeLight: 'rgba(233, 188, 90, 0.5)',
    orangeLighthover: 'rgba(233, 188, 90, 0.8)',

    red: 'rgba(233, 68, 64, 0.5)',
    redHover: 'rgba(233, 68, 64, 0.8)',
    redLight: 'rgba(251, 123, 101, 0.5)',
    redLightHover: 'rgba(251, 123, 101, 0.8)',

    purple: 'rgba(165, 64, 184, 0.5)',
    purpleHover: 'rgba(165, 64, 184, 0.8)',

    call_in: 'rgba(38, 122, 255, 0.5)',
    call_in_hover: 'rgba(38, 122, 255, 0.8)',
    call_out: 'rgba(76, 167, 80, 0.5)',
    call_out_hover: 'rgba(76, 167, 80, 0.8)',
    call_answer: 'rgba(254, 162, 32, 0.5)',
    call_answer_hover: 'rgba(254, 162, 32, 0.8)',
    call_no_answer: 'rgba(233, 68, 64, 0.5)',
    call_no_answer_hover: 'rgba(233, 68, 64, 0.8)',
    call_local: 'rgba(165, 64, 184, 0.5)',
    call_local_hover: 'rgba(165, 64, 184, 0.8)',
    call_unknow: 'rgba(127, 140, 141, 0.5)',
    call_unknow_hover: 'rgba(127, 140, 141, 0.8)',

};

export const linearGradients = {

    tBlue: 'linear-gradient(to top, #01b8fa, #267aff)',
    bBlue: 'linear-gradient(to bottom, #01b8fa, #267aff)',
    rBlue: 'repeating-linear-gradient(rgba(38, 122, 255,0.95), rgba(1, 184, 250,0.95) 70%, rgba(1, 184, 250, 0.95) 30%, rgba(1, 184, 250,0.95) 100%)',

    green: 'linear-gradient(to bottom, #32c991, #4ca750)',
    white: 'linear-gradient(to bottom, #ffffff, #ffffff)',

};
