import { PostService, GetService } from "./Services";
import { DATA_STATUS, DOMAIN } from "../utils/config";

const GetAllWalletERP = () => {
    const url = `${DOMAIN.URL}/wallet/get`;
    return new Promise((res, rej) => {
        GetService(url)
            .then((resService) => {
                res({
                    data: resService,
                    status: DATA_STATUS.SUCCESS,
                });
            })
            .catch((rejService) => {
                res({
                    status: DATA_STATUS.FAILED,
                    err: rejService,
                });
            });
    });
};


export { GetAllWalletERP };