import React, { useState, useEffect } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import HeaderApp from '../../components/header';
import { styles } from './styles';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import { useDispatch, useSelector } from 'react-redux';
import { getAllService, getAllCate, getCateDetail } from '../../redux/action/GetAllAction'
import axios from 'axios';
import { Modal, BottomModal, ModalContent, ModalFooter, ModalButton, ModalTitle } from 'react-native-modals';
const images = {
    1: require('../../assets/images/food.png'),
    2: require('../../assets/images/bill.png'),
    3: require('../../assets/images/car.png'),
    4: require('../../assets/images/shopping.png'),
    5: require('../../assets/images/friend.png'),
    6: require('../../assets/images/entertainment.png'),
    7: require('../../assets/images/plane.png'),
    //education: require('../../assets/images/education.png'),
    8: require('../../assets/images/health.png'),
    9: require('../../assets/images/gift.png'),
    10: require('../../assets/images/home.png'),

}


const SECTIONS = [
    {
        title: 'First',
        content: 'Lorem ipsum...',
    },
    {
        title: 'Second',
        content: 'Lorem ipsum...',
    },
];

function Notification({ navigation }) {
    const hProps = {
        title: 'Service',
        navigation
    }
    const dispatch = useDispatch();
    const token = useSelector((state) => state.AuthReducer.token)
    const allService = useSelector((state) => state.WalletReducer.service)
    const allCate = useSelector((state) => state.WalletReducer.cate)
    console.log("🚀 ~ Notification ~ allCate =>", allCate)

    const [activeSections, setActiveSections] = useState([]);
    const [visible, setVisible] = useState(false);

    async function getService() {
        try {
            const response = await axios.get('http://localhost:3000/service/category',
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response) {
                dispatch(getAllService(response.data[0]))
            }
        } catch (error) {
            //  console.error('error =>', error);
        }
    }

    async function getCate(id) {
        try {
            const response = await axios.get(`http://localhost:3000/service/get/${id}`,
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });

            if (response) {
                dispatch(getAllCate(response.data[0]))
            }
        } catch (error) {
            //console.error('error =>', error);
        }
    }

    useEffect(() => {
        getService();
    }, [])

    const onModalService = (id) => {
        getCate(id)
        setVisible(true)
    }

    const onCate = (item) => {
        dispatch(getCateDetail(item));
        setVisible(false);
        navigation.navigate("SpendScreen");
    }

    return (
        <View >
            <HeaderApp {...hProps} />

            <View style={styles.wrapper}>
                <View style={{ height: "78%" }}>
                    <ScrollView>
                        {
                            allService.map((serv, idx) => {
                                return (
                                    <TouchableOpacity key={idx} onPress={() => onModalService(serv.service_category_id)}>
                                        <View style={styles.flex} >
                                            <Image style={styles.img} source={images[serv.service_category_id]} />
                                            <Text style={styles.name}>{serv.service_category_title}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                {/* <View>
                    <View style={styles.flex}>
                        <Image style={styles.img} source={images.home} />
                        <Text style={styles.name}>Gia đình</Text>
                    </View>
                </View>
                <View>
                    <View style={styles.flex}>
                        <Image style={styles.img} source={images.education} />
                        <Text style={styles.name}>Giáo dục</Text>
                    </View>
                </View>
                <View>
                    <View style={styles.flex}>
                        <Image style={styles.img} source={images.bill} />
                        <Text style={styles.name}>Hoá đơn</Text>
                    </View>
                </View>
                <View>
                    <View style={styles.flex}>
                        <Image style={styles.img} source={images.car} />
                        <Text style={styles.name}>Du lịch</Text>
                    </View>
                </View>
                <View>
                    <View style={styles.flex}>
                        <Image style={styles.img} source={images.gift} />
                        <Text style={styles.name}>quà tặng</Text>
                    </View>
                </View> */}
                {/* <Accordion
                sections={SECTIONS}
                activeSections={activeSections}
                renderHeader={_renderHeader}
                renderContent={_renderContent}
                onChange={_updateSections}
                TouchableHighlight={false}
            /> */}
            </View>

            <BottomModal
                visible={visible}
                modalTitle={<ModalTitle title="Dịch vụ" />}
                onTouchOutside={() => {
                    setVisible(false);
                }}
            >
                <ModalContent>
                    {
                        allCate.map((cate, idx) => {
                            return (
                                <TouchableOpacity key={idx} onPress={() => onCate(cate)}><Text style={styles.serv}>{cate.service_name}</Text></TouchableOpacity>
                            )
                        })
                    }
                </ModalContent>
            </BottomModal>
        </View>
    );
}

export default Notification;