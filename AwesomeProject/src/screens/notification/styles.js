import { StyleSheet } from 'react-native';
import { textColors, colors } from '../../styles';

export const styles = StyleSheet.create({
    wrapper: {
        height: '100%',
        backgroundColor: colors.white,
        paddingHorizontal: 10
    },
    img: {
        width: 70,
        height: 70,
    },
    name: {
        fontWeight: '600',
        fontSize: 16,
        color: textColors.primary,
        marginLeft: 12
    },
    flex: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.white,
        paddingLeft: 12,
        paddingVertical: 10,
        borderBottomWidth: 0.2
    },
    serv: {
        fontSize: 16,
        color: colors.primary,
        fontWeight: "500",
        textAlign: 'center',
        paddingVertical: 14
    }
})