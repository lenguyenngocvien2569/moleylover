import React, { useState } from 'react'
import { Button, Overlay } from 'react-native-elements';
import { View, Image, Dimensions, TextInput, TouchableOpacity, Text } from 'react-native';
import { styles } from './styles';
import I18n from 'react-native-i18n';
import { getLanguages } from 'react-native-i18n';
import { DATA_STATUS } from "../../../utils/config";
import { UserLoginBusiness } from '../../../business/AuthBusiness';
import jwt_decode from "jwt-decode";
import { useDispatch } from 'react-redux';
import { login, token } from '../../../redux/action/AuthAction';

import { UserCircle, Lock, ShowPass, HidePass } from '../../../assets/icons';

const images = {
    logo: require('../../../assets/images/logo2.png'),
}

export default function Login({ navigation }) {
    const [visible, setVisible] = useState(false);
    const [formLogin, setFormLogin] = useState({});
    const [showPassword, setShowPassword] = useState(false);
    const dispatch = useDispatch();

    const toggleOverlay = () => {
        setVisible(!visible);
    };

    const onRegister = () => {
        navigation.navigate('RegisterScreen');
    };

    const onLogin = async (event) => {
        event.preventDefault();
        const { username, password } = formLogin;

        await UserLoginBusiness(username, password).then((response) => {
            if (response.status === DATA_STATUS.SUCCESS) {
                const user = response.data;
                let istoken = user.token;
                let decode = jwt_decode(istoken);
                let userInfo = {
                    username: decode.userName,
                    id: decode.id_user,
                    role: decode.role_id,
                }
                if (user) {
                    dispatch(login(userInfo));
                    dispatch(token(istoken));
                }
            }
        });
    };


    getLanguages().then(languages => {
        console.log(languages); // ['en-US', 'en']
    });

    const SCREEN_WIDTH = Dimensions.get('screen').width;
    return (
        <View style={styles.formInput}>
            {/* Logo Image */}
            <View style={styles.headerLogin}>
                <Image
                    style={{
                        width: SCREEN_WIDTH,
                        height: 'auto',
                        aspectRatio: 3 / 2, //WIDTH/HEIGHT
                        marginBottom: 50,
                    }}
                    source={images.logo}
                />
            </View>

            {/* Input Form */}
            <View style={styles.formInput_elm}>

                {/* Email/Username input box */}
                <View style={styles.inputBox}>
                    <TouchableOpacity onPress={toggleOverlay} >
                        <UserCircle style={styles.icon} />
                    </TouchableOpacity>

                    <TextInput
                        value={formLogin.username}
                        onChangeText={(text) => {
                            setFormLogin({ ...formLogin, username: text });
                        }}
                        style={styles.input}
                        placeholder='username'
                    />
                </View>

                {/* Password input box */}
                <View style={styles.inputBox}>
                    <Lock style={styles.icon} />
                    <TextInput
                        value={formLogin.password}
                        onChangeText={(text) => {
                            setFormLogin({ ...formLogin, password: text });
                        }}
                        style={styles.input}
                        placeholder='password'
                        secureTextEntry={!showPassword ? true : false}
                    />

                    <TouchableOpacity
                        onPress={() => {
                            setShowPassword(!showPassword);
                        }}>
                        {!showPassword ? (
                            <ShowPass style={styles.iconPassShow} />
                        ) : (
                            <HidePass style={styles.iconPass} />
                        )}
                    </TouchableOpacity>
                </View>
            </View>
            <TouchableOpacity onPress={onLogin} style={styles.btn_Login}>
                <Text style={styles.text_btn}>Đăng nhập</Text>
            </TouchableOpacity>

            <View style={styles.text_change}>
                <Text>Bạn chưa có tài khoản?</Text>
                <TouchableOpacity onPress={onRegister}>
                    <Text style={styles.text_for}>Đăng kí</Text>
                </TouchableOpacity>
            </View>

            <Overlay isVisible={visible} onBackdropPress={toggleOverlay} style={styles.overlay}>
                <Text>{I18n.t('greeting')}</Text>
            </Overlay>
        </View>
    )
}

I18n.fallbacks = true;

I18n.translations = {
    en: {
        greeting: 'Hi!',
    },
    vi: {
        greeting: 'nammmmm!',
    },
};