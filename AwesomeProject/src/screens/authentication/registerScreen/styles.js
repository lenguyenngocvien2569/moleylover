import { StyleSheet } from 'react-native';
import { textColors, colors } from '../../../styles';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },

    headerLogin: {
        width: '100%',
        height: 150,
        alignItems: 'center',
        justifyContent: 'center',
    },

    formInput: {
        width: '100%',
        height: '100%',
        padding: 20,
    },

    div: {
        height: '80%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },

    formInput_elm: {
        marginTop: 10,
    },

    input: {
        width: '80%',
        height: 50,

        paddingHorizontal: 10,

    },

    btn_Login: {
        width: '100%',
        height: 50,
        backgroundColor: colors.green,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },

    loginGoogle: {
        width: '100%',
        height: 50,
        marginTop: 20,
        backgroundColor: '#db4437',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },

    text_btn: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
    },

    text_change: {
        marginTop: 20,
        flexDirection: 'row',
    },

    text_for: {
        marginLeft: 15,
        color: textColors.success,
        fontWeight: 'bold',
    },

    inputBox: {
        flexDirection: 'row',
        borderWidth: 1.5,
        borderRadius: 20,
        marginBottom: 22,
        height: 50,
        paddingLeft: 10,
        alignItems: 'center',
    },

    icon: {
        paddingLeft: 5,
        fontSize: 30,
        color: 'gray',
    },

    iconPass: {
        fontSize: 23,
        color: 'gray',
        paddingRight: 5,
    },

    iconPassShow: {
        fontSize: 25,
        color: 'gray',
        paddingRight: 5,
    },

    forwardPosition: {
        position: 'absolute',
        top: 10,
        left: 10,
        flexDirection: 'row',
    },

    forwardBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
    },

    forwardIcon: {
        fontSize: 16,
        marginRight: 5,
        color: colors.green,
    },

    forwardText: {
        fontWeight: 'bold',
        fontSize: 14,
        color: colors.primary,
    },

    title: {
        fontSize: 30,
        fontWeight: 'bold',
        marginBottom: 10,
        color: colors.green,
    },

    test: {
        position: 'relative',
    },

    version: {
        position: 'absolute',
        bottom: 10,
    },

    overlay: {
        backgroundColor: 'red',
    }
});