import React, { useState } from 'react'
import { View, Image, Dimensions, TextInput, TouchableOpacity, Text } from 'react-native';
import { styles } from './styles';
import HeaderApp from '../../../components/header';
import axios from 'axios';
import { Modal, ModalContent, ModalFooter, ModalButton, ModalTitle } from 'react-native-modals';

import { IconArrowLeft, UserCircle, Lock, ShowPass, HidePass } from '../../../assets/icons';

export default function RegisterScreen({ navigation }) {
    const [showPassword, setShowPassword] = useState(false);
    const [formRegister, setFormRegister] = useState({});
    const [visible, setVisible] = useState(false);

    const onGoBack = () => {
        navigation.goBack();
    };
    let hProps = {
        isNone: true
    }

    async function handleRegister() {
        try {
            const response = await axios.post('http://localhost:3000/user/register',
                {
                    userName: formRegister.username,
                    password: formRegister.password,
                });
            if (response.data.status === 'success') {
                setVisible(true)
            }
        } catch (error) {
            console.error('error =>', error);
        }
    }

    const onRegister = () => {
        setVisible(false)
        onGoBack();
    }

    return (
        <>
            <HeaderApp  {...hProps} />
            <View style={styles.test}>
                <View style={styles.formInput}>
                    <View style={styles.forwardPosition}>
                        <TouchableOpacity onPress={onGoBack} style={styles.forwardBtn}>
                            <IconArrowLeft style={styles.forwardIcon} />
                            <Text style={styles.forwardText}>back</Text>
                            {/* <Text style={styles.forwardText}>{I18n.t('back')}</Text> */}
                        </TouchableOpacity>
                    </View>
                    {/* Input Form */}
                    <View style={styles.div}>
                        <Text style={styles.title}>Đăng kí</Text>

                        <View style={styles.formInput_elm}>
                            {/* URL input box */}
                            {/* Email/Username input box */}
                            <View style={styles.inputBox}>
                                <UserCircle style={styles.icon} />
                                <TextInput
                                    style={styles.input}
                                    placeholder="Username"
                                    value={formRegister.username}
                                    onChangeText={(text) => {
                                        setFormRegister({ ...formRegister, username: text });
                                    }}
                                // placeholder={I18n.t('username')}
                                />
                            </View>

                            {/* Password input box */}
                            <View style={styles.inputBox}>
                                <Lock style={styles.icon} />
                                <TextInput
                                    value={formRegister.password}
                                    onChangeText={(text) => {
                                        setFormRegister({ ...formRegister, password: text });
                                    }}
                                    style={styles.input}
                                    placeholder='password'
                                    secureTextEntry={!showPassword ? true : false}
                                />

                                <TouchableOpacity
                                    onPress={() => {
                                        setShowPassword(!showPassword);
                                    }}>
                                    {!showPassword ? (
                                        <ShowPass style={styles.iconPassShow} />
                                    ) : (
                                        <HidePass style={styles.iconPass} />
                                    )}
                                </TouchableOpacity>
                            </View>
                        </View>

                        {/* Reset Button */}
                        <TouchableOpacity style={styles.btn_Login} onPress={() => { handleRegister() }}>
                            <Text style={styles.text_btn}>Đăng kí</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <Modal
                    visible={visible}
                    width={250}
                    onTouchOutside={() => {
                        setVisible(false)
                    }}
                    modalTitle={<ModalTitle title="Thông báo" />}
                    footer={
                        <ModalFooter>
                            <ModalButton
                                text="OK"
                                onPress={() => onRegister()}
                            />
                        </ModalFooter>
                    }
                >
                    <ModalContent>
                        <Text style={{ textAlign: 'center' }}>Đăng kí thành công</Text>
                    </ModalContent>
                </Modal>
            </View>
        </>
    );
}


