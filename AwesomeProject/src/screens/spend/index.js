import React, { useState } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import HeaderApp from '../../components/header';
import { styles } from './styles';
import axios from 'axios';
import { getAllWallet, getAllExpense } from '../../redux/action/GetAllAction';
import { useSelector, useDispatch } from 'react-redux'
import { Modal, ModalContent, ModalFooter, ModalButton, BottomModal, ModalTitle } from 'react-native-modals';

Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function SpendScreen({ navigation }) {
    const hProps = {
        title: 'Spend Screen',
        navigation
    }
    const token = useSelector((state) => state.AuthReducer.token)
    const cateDetail = useSelector((state) => state.WalletReducer.cateDetail)
    const allWallet = useSelector((state) => state.WalletReducer.wallets)
    const dispatch = useDispatch();
    // const [form, setForm] = useState({
    //     name: detail.wallet_name,
    //     money: detail.amount.toString()
    // });
    const [visible, setVisible] = useState(false);
    const [visibleW, setVisibleW] = useState(false);
    const [wl, setWl] = useState({});
    const [money, setMoney] = useState('');

    async function handleCreate() {
        try {
            const response = await axios.post('http://localhost:3000/wallet/spend',
                {
                    walletId: wl.wallet_id,
                    spend: parseInt(money),
                    service_id: cateDetail.service_id
                },
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response.data.status === 'success') {
                console.log("🚀 ~ handleCreate ~ response =>>", response)
                setVisible(true)
                getWallet()
            }
        } catch (error) {
            // console.error('error =>', error);
        }
    }

    async function getExpense() {
        try {
            const response = await axios.get('http://localhost:3000/wallet/spend/get/7',
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response.data.status === 'success') {
                dispatch(getAllExpense(response.data.data))
            }
        } catch (error) {
            //console.error('error =>', error);
        }
    }

    async function getWallet() {
        try {
            const response = await axios.get('http://localhost:3000/wallet/get',
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response.data.status === 'success') {
                dispatch(getAllWallet(response.data.data))
            }
        } catch (error) {
            //console.error('error =>', error);
        }
    }

    const handleAdd = () => {
        getExpense();
        setVisible(false);
        setVisibleW(false);
        setMoney('');
        setWl({});
        navigation.navigate('DashboardScreen')
    }
    const onWall = (item) => {
        setWl(item);
        setVisibleW(false);
    }



    return (
        <View >
            <HeaderApp {...hProps} />

            <View style={styles.wrapper}>
                <View style={styles.tit}>
                    <Text style={styles.titleL}>Thanh Toán: </Text>
                    <Text style={styles.titleR}>{cateDetail.service_name}</Text>
                </View>


                <View style={styles.container}>
                    <TouchableOpacity style={styles.text} onPress={() => setVisibleW(true)}><Text style={styles.textT}>chọn ví:</Text></TouchableOpacity>
                    <TextInput
                        style={styles.input}
                        placeholder="Nhập tên ví"
                        value={wl?.wallet_name}
                        onChangeText={(text) => {
                            setWl({ ...wl, wallet_name: text });
                        }}
                    />
                </View>
                <View style={styles.container}>
                    <Text style={styles.text}>Số tiền:</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="Nhập Số tiền"
                        value={money}
                        onChangeText={(text) => {
                            setMoney(text);
                        }}
                    />
                </View>
                <TouchableOpacity onPress={() => handleCreate()}><View style={styles.btn}><Text style={styles.btnText}>Xác nhận</Text></View></TouchableOpacity>
            </View>

            <Modal
                visible={visible}
                width={250}
                onTouchOutside={() => {
                    setVisible(false);
                }}
                footer={
                    <ModalFooter>
                        <ModalButton
                            text="OK"
                            onPress={() => handleAdd()}
                        />
                    </ModalFooter>
                }
            >
                <ModalContent>
                    <Text style={{ textAlign: 'center' }}>Thanh toán thành công</Text>
                </ModalContent>
            </Modal>

            <BottomModal
                visible={visibleW}
                height={350}
                onTouchOutside={() => {
                    setVisibleW(false);
                }}
                modalTitle={<ModalTitle title="Ví của bạn" />}
            >
                <ModalContent>
                    <View>
                        <ScrollView>
                            {allWallet.map((wl, idx) => {
                                return (
                                    <TouchableOpacity onPress={() => onWall(wl)} key={idx}>
                                        <View style={styles.mdFlex}>
                                            <Text style={styles.mdText}>{wl.wallet_name}</Text>
                                            <Text style={styles.mdText}>{wl?.amount?.format()} vnđ</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                    </View>
                </ModalContent>
            </BottomModal>
        </View>
    );
}

export default SpendScreen;