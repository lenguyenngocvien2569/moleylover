import { StyleSheet } from 'react-native';
import { textColors, colors } from '../../styles';

export const styles = StyleSheet.create({
    wrapper: {
        height: '100%',
        backgroundColor: colors.white,
        paddingHorizontal: 20,
        paddingTop: 30,
    },
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 16,
    },
    text: {
        fontWeight: '600',
        fontSize: 16,
        color: textColors.light,
        marginRight: 12,
        width: "17%",
    },
    flex: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.white,
        paddingLeft: 12,
        paddingVertical: 10,
        borderBottomWidth: 0.2
    },
    input: {
        width: "80%",
        borderWidth: 1,
        borderColor: colors.green,
        borderRadius: 6,
        paddingHorizontal: 16,
        paddingVertical: 8,
    },
    btn: {
        backgroundColor: colors.green,
        alignSelf: 'center',
        width: 100,
        paddingVertical: 8,
        borderRadius: 6,
    },
    btnText: {
        fontSize: 16,
        color: textColors.white,
        fontWeight: '600',
        textAlign: 'center',
    }

})