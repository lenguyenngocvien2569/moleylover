import React, { useState } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import HeaderApp from '../../components/header';
import { styles } from './styles';
import axios from 'axios';
import { getAllWallet } from '../../redux/action/GetAllAction';
import { useSelector, useDispatch } from 'react-redux'
import { Modal, ModalContent, ModalFooter, ModalButton } from 'react-native-modals';

function EditWallet({ navigation }) {
    const hProps = {
        title: 'Edit Wallet',
        navigation
    }
    const token = useSelector((state) => state.AuthReducer.token)
    const detail = useSelector((state) => state.WalletReducer.detail)
    console.log("🚀 ~ EditWallet ~ detail ========>", detail)
    const dispatch = useDispatch();
    const [form, setForm] = useState({
        name: detail.wallet_name,
        money: detail.amount.toString()
    });
    const [visible, setVisible] = useState(false);

    async function handleCreate() {
        try {
            const response = await axios.post('http://localhost:3000/wallet/update',
                {
                    wallet_id: detail.wallet_id,
                    wallet_name: form.name,
                    amount: parseInt(form.money),
                },
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response.data.status === 'success') {
                setVisible(true)
                getWallet()
            }
        } catch (error) {
            //   console.error('error =>', error);
        }
    }

    async function getWallet() {
        try {
            const response = await axios.get('http://localhost:3000/wallet/get',
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response.data.status === 'success') {
                dispatch(getAllWallet(response.data.data))
            }
        } catch (error) {
            //   console.error('error =>', error);
        }
    }

    const handleAdd = () => {
        navigation.navigate('DashboardScreen')
        setVisible(false);
        setForm({});
    }

    return (
        <View >
            <HeaderApp {...hProps} />

            <View style={styles.wrapper}>
                <View style={styles.container}>
                    <Text style={styles.text}>Tên ví:</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="Nhập tên ví"
                        value={form.name}
                        onChangeText={(text) => {
                            setForm({ ...form, name: text });
                        }}
                    />
                </View>
                <View style={styles.container}>
                    <Text style={styles.text}>Số tiền:</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="Nhập Số tiền"
                        value={form.money}
                        onChangeText={(text) => {
                            setForm({ ...form, money: text });
                        }}
                    />
                </View>
                <TouchableOpacity onPress={() => handleCreate()}><View style={styles.btn}><Text style={styles.btnText}>Chỉnh sửa</Text></View></TouchableOpacity>
            </View>

            <Modal
                visible={visible}
                width={250}
                onTouchOutside={() => {
                    setVisible(false);
                }}
                footer={
                    <ModalFooter>
                        <ModalButton
                            text="OK"
                            onPress={() => handleAdd()}
                        />
                    </ModalFooter>
                }
            >
                <ModalContent>
                    <Text style={{ textAlign: 'center' }}>Chỉnh sửa ví thành công</Text>
                </ModalContent>
            </Modal>
        </View>
    );
}

export default EditWallet;