
import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import HeaderApp from '../../../components/header';
import { CheckBox } from 'react-native-elements';
import { textColors, colors } from '../../../styles';

const Settings = ({ navigation }) => {
    const [checked, setChecked] = useState(false);
    const hProps = {
        title: 'Setting',
        navigation
    }
    return (
        <>
            <HeaderApp {...hProps} />
            <ScrollView>
                <CheckBox
                    title='Tiếng việt'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={checked}
                    onPress={() => setChecked(!checked)}
                    containerStyle={{ backgroundColor: colors.bgColor }}
                />
                <CheckBox
                    title='Tiếng anh'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={false}
                    onPress={() => setChecked(!checked)}
                    containerStyle={{ backgroundColor: colors.bgColor }}
                />
            </ScrollView>
        </>
    )
}
export default Settings;