import React from 'react';
import { View, Text } from 'react-native';
import HeaderApp from '../../../components/header'
function DashboardDetail({ navigation }) {
    const headerProps = {
        title: 'Dashboard Detail',
        isBack: true,
        navigation
    }
    return (
        <>
            <HeaderApp {...headerProps} />

            <View style={{ flex: 1 }}>
                <Text>Dashboard detail</Text>
            </View>
        </>

    );
}

export default DashboardDetail;