import { StyleSheet } from 'react-native';
import { textColors, colors } from '../../../../styles';

export const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: colors.bgColor,
    },
    searchBox: {
        backgroundColor: '#fff',

        marginHorizontal: 10,
        marginVertical: 10,

        paddingHorizontal: 12,
        paddingVertical: 8,

        borderColor: '#000',
        borderWidth: 0.8,
        borderRadius: 10,
    },
    wallet: {
        width: '94%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginLeft: "auto",
        marginRight: "auto",
        marginVertical: 10,

        backgroundColor: colors.white,
        paddingHorizontal: 12,
        paddingVertical: 8,

        borderRadius: 8,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    icon: {
        fontSize: 20,
        color: 'gray',
    },
    name: {
        fontWeight: '600',
        fontSize: 16,
        color: textColors.primary,
        paddingBottom: 6,
    },
    money: {
        fontSize: 16,
        color: textColors.light,
    },
    effete: {
        fontSize: 16,
        color: textColors.danger,
    },
    itemPop: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: 'red'
    },
    textModal: {
        fontSize: 16,
        color: textColors.light,
        paddingBottom: 8,
    },
    title: {
        fontWeight: "600",
    }
})