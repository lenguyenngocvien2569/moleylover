

import React, { useState, useMemo, useEffect } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Dimensions, TextInput, FlatList, SafeAreaView } from 'react-native';
import { IconMenu, } from '../../../../assets/icons';
import { styles } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { getAllWallet, getWalletDetail } from '../../../../redux/action/GetAllAction';
import { Modal, ModalContent, ModalFooter, ModalButton, ModalTitle } from 'react-native-modals';
import { Menu, MenuOptions, MenuOption, MenuTrigger, } from 'react-native-popup-menu';

const WalletList = ({ navigation }) => {
    const dispatch = useDispatch();

    const token = useSelector((state) => state.AuthReducer.token)
    const allWallet = useSelector((state) => state.WalletReducer.wallets)

    const [search, setSearch] = useState('');
    const [id, setId] = useState('');
    const [walletDetail, setWalletDetail] = useState({});
    const [date, setDate] = useState('');
    const [visible, setVisible] = useState({
        delete: false,
        info: false,
    });

    Number.prototype.format = function (n, x) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };

    async function getWallet() {
        try {
            const response = await axios.get('http://localhost:3000/wallet/get',
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response.data.status === 'success') {
                dispatch(getAllWallet(response.data.data))
            }
        } catch (error) {
            //  console.error('error =>', error);
        }
    }


    useEffect(() => {
        getWallet();
    }, [])

    let lstWallet = useMemo(() => {
        let result = allWallet.filter((wl) => {
            return wl.wallet_name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
        });
        return result;
    }, [allWallet, search]);

    const onVisible = (key) => {
        setVisible({
            ...visible,
            [key]: !visible[key],
        })
    }

    const onModalDelete = (id) => {
        onVisible('delete')
        setId(id)
    }

    const onModalInfo = (item) => {
        setWalletDetail(item);
        var date = new Date(item.created_at);
        let nam = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear();
        setDate(nam);
        onVisible('info');
    }

    const onUpdate = (wallet) => {
        dispatch(getWalletDetail(wallet))
        navigation.navigate("EditScreen")
    }

    async function handleDelete() {
        try {
            const response = await axios.post('http://localhost:3000/wallet/delete',
                {
                    wallet_id: id,
                },
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response.data.status === 'success') {
                getWallet();
                setVisible(false)
            }
        } catch (error) {
            // console.error('error =>', error);
        }
    }

    return (
        <View style={styles.wrapper}>
            <View>
                <TextInput style={styles.searchBox} placeholder='Tìm kiếm'
                    onChangeText={(text) => {
                        setSearch(text);
                    }} />

                <View style={{ height: "91%" }}>
                    <ScrollView >
                        {
                            lstWallet.map((wallet, idx) => {
                                return (
                                    <View style={styles.wallet} key={idx}>
                                        <View>
                                            <Text style={styles.name}>{wallet.wallet_name}</Text>
                                            <Text style={styles.money}>Khả dụng: <Text>{wallet.amount.format()} vnđ</Text></Text>
                                        </View>

                                        {/* <TouchableOpacity onPress={() => onModal(wallet.wallet_id)}></TouchableOpacity> */}
                                        <Menu>
                                            <MenuTrigger><IconMenu style={styles.icon} /></MenuTrigger>
                                            <MenuOptions>
                                                <MenuOption onSelect={() => onModalInfo(wallet)} text='Thông tin ví' />
                                                <MenuOption onSelect={() => onUpdate(wallet)} text='Chỉnh sửa' />
                                                <MenuOption onSelect={() => onModalDelete(wallet.wallet_id)} >
                                                    <Text style={{ color: 'red' }}>Xoá</Text>
                                                </MenuOption>
                                            </MenuOptions>
                                        </Menu>
                                    </View>
                                )
                            })
                        }


                        {/* <View style={styles.wallet}>
                        <View>
                            <Text style={styles.name}>Ví mua sắm</Text>
                            <Text style={styles.money}>Khả dụng: <Text style={styles.effete}>0 vnđ</Text> </Text>
                        </View>

                        <IconMenu style={styles.icon} />
                    </View>

                    <View style={styles.wallet}>
                        <View>
                            <Text style={styles.name}>ví tiết kiệm</Text>
                            <Text style={styles.money}>Khả dụng: 1.500.000 vnđ</Text>
                        </View>

                        <IconMenu style={styles.icon} />
                    </View> */}
                    </ScrollView>
                </View>
            </View>

            <Modal
                visible={visible.delete}
                width={250}
                onTouchOutside={() => {
                    onVisible('delete');
                }}
                footer={
                    <ModalFooter>
                        <ModalButton
                            style={{ color: 'red' }}
                            text="Đồng ý"
                            onPress={() => handleDelete()}
                        />
                        <ModalButton
                            text="Huỷ bỏ"
                            onPress={() => onVisible('delete')}
                        />
                    </ModalFooter>
                }
            >
                <ModalContent>
                    <Text style={{ textAlign: 'center' }}>Bạn có muốn xoá?</Text>
                </ModalContent>
            </Modal>

            <Modal
                visible={visible.info}
                width={250}
                modalTitle={<ModalTitle title="Thông tin ví" />}
                onTouchOutside={() => {
                    onVisible('delete');
                }}
                footer={
                    <ModalFooter>
                        <ModalButton
                            text="OK"
                            onPress={() => onVisible('info')}
                        />
                    </ModalFooter>
                }
            >
                <ModalContent>
                    <View style={{ paddingTop: 30 }}>
                        <Text style={styles.textModal}><Text style={styles.title}>Tên ví: </Text>{walletDetail.wallet_name}</Text>
                        <Text style={styles.textModal}><Text style={styles.title}>Mã ví: </Text>{walletDetail.wallet_id}</Text>
                        <Text style={styles.textModal}><Text style={styles.title}>Số tiền: </Text>{walletDetail?.amount?.format()} vnđ</Text>
                        <Text style={styles.textModal}><Text style={styles.title}>Ngày tạo: </Text>{date}</Text>
                    </View>
                </ModalContent>
            </Modal>
        </View >
    )
}
export default WalletList;