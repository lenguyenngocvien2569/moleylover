import { StyleSheet } from 'react-native';
import { textColors, colors } from '../../../styles';

export const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: colors.bgColor,
        paddingBottom: 10,
    },
    time: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',

        width: '95%',
        marginRight: 'auto',
        marginLeft: 'auto',
        marginTop: 16,
        paddingHorizontal: 24,
        paddingVertical: 16,
        backgroundColor: colors.white,
        borderRadius: 8,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    text: {
        fontSize: 16,
        color: colors.primary,
    },
    container: {
        width: '95%',
        marginRight: 'auto',
        marginLeft: 'auto',
        marginTop: 24,

        paddingVertical: 16,
        paddingRight: 24,
        paddingLeft: 20,
        backgroundColor: colors.white,
        borderRadius: 8,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
    },
    item: {
        borderBottomWidth: 1,
        borderColor: colors.gray,
    },
    info: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    img: {
        width: 60,
        height: 60,
        marginRight: 8,
    },
    service: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    serviceText: {
        fontSize: 16,
        color: textColors.light,
        fontWeight: '600'
    },
    desc: {
        paddingLeft: 6,
        paddingTop: 8,
    },
    pad: {
        fontSize: 16,
        color: textColors.light,
        paddingBottom: 6,
    }
})