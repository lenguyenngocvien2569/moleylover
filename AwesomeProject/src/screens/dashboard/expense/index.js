import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import { getAllExpense, getAllService } from '../../../redux/action/GetAllAction';
import { styles } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';

const images = {
    1: require('../../../assets/images/food.png'),
    2: require('../../../assets/images/bill.png'),
    3: require('../../../assets/images/car.png'),
    4: require('../../../assets/images/shopping.png'),
    5: require('../../../assets/images/friend.png'),
    6: require('../../../assets/images/entertainment.png'),
    7: require('../../../assets/images/plane.png'),
    8: require('../../../assets/images/health.png'),
    9: require('../../../assets/images/gift.png'),
    10: require('../../../assets/images/home.png'),

}

const Expense = ({ navigation }) => {
    const dispatch = useDispatch();
    const token = useSelector((state) => state.AuthReducer.token)
    const allService = useSelector((state) => state.WalletReducer.service)
    const allExpense = useSelector((state) => state.WalletReducer.expense)

    Number.prototype.format = function (n, x) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };

    async function getExpense() {
        try {
            const response = await axios.get('http://localhost:3000/wallet/spend/get/7',
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response.data.status === 'success') {
                dispatch(getAllExpense(response.data.data))
            }
        } catch (error) {
            //console.error('error =>', error);
        }
    }

    async function getService() {
        try {
            const response = await axios.get('http://localhost:3000/service/category',
                {
                    headers: {
                        'Authorization': `${token}`
                    }
                });
            if (response) {
                dispatch(getAllService(response.data[0]))
            }
        } catch (error) {
        }
    }


    useEffect(() => {
        getExpense();
        getService();
    }, [])

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [day, month, year].join('/');
    }

    const getName = (id) => {
        var name = ''
        allService?.map(item => {
            if (item.service_category_id === id) {
                name = item.service_category_title
            }
        })
        return name;
    }

    return (
        <View style={styles.wrapper}>
            <ScrollView>
                <View style={styles.time}>
                    <Text style={styles.text}>Tháng này</Text>
                    <Text style={styles.text}>Đã chi tiêu: {allExpense?.total?.format()} vnđ</Text>
                </View>

                <View style={{ marginBottom: 16 }}>
                    {
                        allExpense?.detail?.map((exp, idx) => {
                            return (
                                <View style={styles.container} key={idx}>
                                    <View style={styles.info}>
                                        <View style={styles.service}>
                                            <Image source={images[exp?.service_id]} style={styles.img} />
                                            <Text style={styles.serviceText}>{getName(exp?.service_id)}</Text>
                                        </View>

                                        <Text style={styles.text}>{exp?.spend?.format()} vnđ</Text>
                                    </View>
                                    <View style={styles.desc}>
                                        <Text style={styles.pad}>Dịch vụ: {exp?.service_name}</Text>
                                        <Text style={styles.pad}>Thời gian: {formatDate(exp?.created_at)}</Text>
                                    </View>
                                </View>
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>
    )
}
export default Expense;