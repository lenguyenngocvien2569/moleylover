import React, { useState, useEffect } from 'react';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { View, Text, TouchableOpacity, useWindowDimensions } from 'react-native';
import HeaderApp from '../../components/header';
import WalletList from './wallet/list';
import Expense from './expense';
import { styles } from './styles';
import { colors } from '../../styles'
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { getAllWallet } from '../../redux/action/GetAllAction';



function DashboardScreen({ navigation, route }) {
    const dispatch = useDispatch();
    const token = useSelector((state) => state.AuthReducer.token)

    // async function getWallet() {
    //     try {
    //         const response = await axios.get('http://localhost:3000/wallet/get',
    //             {
    //                 headers: {
    //                     'Authorization': `${token}`
    //                 }
    //             });
    //         if (response.data.status === 'success') {
    //             dispatch(getAllWallet(response.data.data))
    //         }
    //     } catch (error) {
    //         console.error('error =>', error);
    //     }
    // }


    // getWallet();

    const FirstRoute = () => (
        <View style={{ flex: 1, backgroundColor: colors.bgColor }} >
            <Expense />
        </View>
    );

    const SecondRoute = () => (
        <View style={{ flex: 1, backgroundColor: colors.bgColor }} >
            <WalletList navigation={navigation} />
        </View>
    );

    const ThirdRoute = () => (
        <View style={{ flex: 1, backgroundColor: 'blue' }} />
    );

    const layout = useWindowDimensions();

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: ' Chi tiêu' },
        { key: 'second', title: 'Ví tiền' },
        { key: 'third', title: 'Thống kê' },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
        third: ThirdRoute,
    });



    const onGoToDetail = () => {
        navigation.navigate('DashboardDetail');
    };

    const headerProps = {
        title: 'Dashboard',
        navigation
    }

    return (
        <View style={{ flex: 1 }}>
            <HeaderApp {...headerProps} />

            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                style={styles.container}
                renderTabBar={props => <TabBar {...props} style={{ backgroundColor: '#4ca750' }} />}
            />
        </View>
    );

}

export default DashboardScreen;