import 'react-native-gesture-handler';
import { ModalPortal } from 'react-native-modals'
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import RootNavigation from './routes/root/RootNavigation';
import { MenuProvider } from 'react-native-popup-menu';
import { Provider } from 'react-redux';
import Store from './redux/Store';

export default function App() {
  return (
    <SafeAreaProvider>
      <Provider store={Store}>
        <NavigationContainer>
          <MenuProvider>
            <RootNavigation />
            <ModalPortal />
          </MenuProvider>
        </NavigationContainer>
      </Provider>
    </SafeAreaProvider>
  );
}