import React from 'react';
import { View, Image, Dimensions, TextInput, TouchableOpacity, Text } from 'react-native';
import { Header, ThemeProvider } from 'react-native-elements';
import { IconBar, IconArrowLeft } from '../../assets/icons';
import { textColors, colors } from '../../styles';
export default HeaderApp = (props) => {

    const onDrawer = () => {
        props.isBack ? props.navigation?.goBack() : props.navigation?.openDrawer();
    };

    const _renderIconLeft = () => {
        return (
            <TouchableOpacity onPress={onDrawer}>
                {
                    props.isBack ? <IconArrowLeft size={18} color={textColors.white} /> : <IconBar size={18} color={textColors.white} />
                }
            </TouchableOpacity>
        )
    }
    return (
        <Header
            leftComponent={props.isNone || <_renderIconLeft />}
            centerComponent={{ text: props.title, style: { color: textColors.white } }}
            containerStyle={{ backgroundColor: props.isNone ? colors.bgColor : colors.green }}
        />
    )
}
