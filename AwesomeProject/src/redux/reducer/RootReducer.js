
import { combineReducers } from "redux";
import AuthReducer from "../reducer/AuthReducer";
import WalletReducer from "../reducer/GetAllReducer";

const myReducer = combineReducers({ AuthReducer, WalletReducer });

export default myReducer;