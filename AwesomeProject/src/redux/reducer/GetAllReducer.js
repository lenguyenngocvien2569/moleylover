import { NAME_SCREEN } from "../NameAction";
import { DATA_STATUS } from "../../utils/config";

const initState = {
  detail: {},
  wallets: [],
  service: [],
  cate: [],
  cateDetail: {},
  expense: {},
  status: DATA_STATUS.NONE,
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case NAME_SCREEN.WALLET_SCREEN.GET_WALLETS:
      return {
        ...state,
        wallets: action.payload.wallets,
        status: DATA_STATUS.SUCCESS,
      };
    case NAME_SCREEN.WALLET_SCREEN.GET_EXPENSE:
      return {
        ...state,
        expense: action.payload.expense,
        status: DATA_STATUS.SUCCESS,
      };
    case NAME_SCREEN.WALLET_SCREEN.WALLET_DETAIl:
      return {
        ...state,
        detail: action.payload.detail,
        status: DATA_STATUS.SUCCESS,
      };
    case NAME_SCREEN.WALLET_SCREEN.GET_SERVICE:
      return {
        ...state,
        service: action.payload.service,
        status: DATA_STATUS.SUCCESS,
      };
    case NAME_SCREEN.WALLET_SCREEN.GET_CATEGORY:
      return {
        ...state,
        cate: action.payload.cate,
        status: DATA_STATUS.SUCCESS,
      };
    case NAME_SCREEN.WALLET_SCREEN.CATE_DETAIl:
      return {
        ...state,
        cateDetail: action.payload.cateDetail,
        status: DATA_STATUS.SUCCESS,
      };

    //get DETAIL
    // case NAME_PAGE.JOB_DETAIL_PAGE.GET_JOB_DETAIL:
    //   return {
    //     ...state,
    //     jobDetail: action.payload.jobDetail,
    //     status: DATA_STATUS.SUCCESS,
    //   };

    default:
      return state;
  }
};

export default reducer;
