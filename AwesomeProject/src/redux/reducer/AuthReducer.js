import { NAME_SCREEN } from "../NameAction";
import { DATA_STATUS } from "../../utils/config";

const initState = {
    token: '',
    isSplash: true,
    userInfo: {
        id: 0,
        name: "",
        address: "",
        phoneNumber: "",
        email: "",
        roles: "",
    },
    status: DATA_STATUS.NONE,
};

const reducer = (state = initState, action) => {
    switch (action.type) {

        case NAME_SCREEN.SPLASH.TO_LOGIN:
            return {
                ...state,
                isSplash: false
            }

        case NAME_SCREEN.AUTHENTICATION.USER_LOGIN:
            return {
                ...state,
                userInfo: action.payload.userInfo,
                status: DATA_STATUS.SUCCESS,
            };

        case NAME_SCREEN.AUTHENTICATION.TOKEN:
            return {
                ...state,
                token: action.payload.token,
                status: DATA_STATUS.SUCCESS,
            };

        case NAME_SCREEN.AUTHENTICATION.USER_LOGOUT:
            return {
                token: '',
                isSplash: true,
                userInfo: {
                    id: 0,
                    name: "",
                    address: "",
                    phoneNumber: "",
                    email: "",
                    roles: "",
                },
                status: DATA_STATUS.NONE,
            };

        default:
            return state;
    }
};

export default reducer;