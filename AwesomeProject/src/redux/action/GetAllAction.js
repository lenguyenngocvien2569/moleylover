import { NAME_SCREEN } from "../NameAction";

//Get POST
export const getAllExpense = (expense) => {
  return {
    type: NAME_SCREEN.WALLET_SCREEN.GET_EXPENSE,
    payload: { expense },
  };
};
export const getAllWallet = (wallets) => {
  return {
    type: NAME_SCREEN.WALLET_SCREEN.GET_WALLETS,
    payload: { wallets },
  };
};

export const getWalletDetail = (detail) => {
  return {
    type: NAME_SCREEN.WALLET_SCREEN.WALLET_DETAIl,
    payload: { detail },
  };
};
export const getCateDetail = (cateDetail) => {
  return {
    type: NAME_SCREEN.WALLET_SCREEN.CATE_DETAIl,
    payload: { cateDetail },
  };
};

export const getAllService = (service) => {
  return {
    type: NAME_SCREEN.WALLET_SCREEN.GET_SERVICE,
    payload: { service },
  };
};
export const getAllCate = (cate) => {
  return {
    type: NAME_SCREEN.WALLET_SCREEN.GET_CATEGORY,
    payload: { cate },
  };
};

//Get DETAIL of 1 job
// export const getJobDetail = (jobDetail) => {
//   return {
//     type: NAME_PAGE.JOB_DETAIL_PAGE.GET_JOB_DETAIL,
//     payload: { jobDetail },
//   };
// };
