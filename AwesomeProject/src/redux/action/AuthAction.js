
import { NAME_SCREEN } from "../NameAction";

export const screenSwitching = (userInfo) => {
    return {
        type: NAME_SCREEN.SPLASH.TO_LOGIN,
    };
};


export const login = (userInfo) => {
    return {
        type: NAME_SCREEN.AUTHENTICATION.USER_LOGIN,
        payload: { userInfo },
    };
};

export const token = (token) => {
    return {
        type: NAME_SCREEN.AUTHENTICATION.TOKEN,
        payload: { token },
    };
};

export const logout = () => {
    return {
        type: NAME_SCREEN.AUTHENTICATION.USER_LOGOUT,
    };
};