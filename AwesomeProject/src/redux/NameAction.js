export const NAME_SCREEN = {
    SPLASH: {
        TO_LOGIN: "TO_LOGIN",
    },

    WALLET_SCREEN: {
        GET_WALLETS: "GET_WALLETS",
        WALLET_DETAIl: "WALLET_DETAIl",
        GET_SERVICE: "GET_SERVICE",
        GET_CATEGORY: "GET_CATEGORY",
        CATE_DETAIl: "CATE_DETAIl",
        GET_EXPENSE: "GET_EXPENSE",
    },

    AUTHENTICATION: {
        USER_LOGIN: "USER_LOGIN",
        USER_LOGOUT: "USER_LOGOUT",
        TOKEN: "TOKEN"
    },
};