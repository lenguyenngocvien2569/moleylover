import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import { IconTachometer, IconAddressBook } from '../../../assets/icons';
import { styles } from './styles';

function DrawerBody({ navigation }) {
    const onGoToDashboard = () => {
        navigation.navigate('DashboardScreen', {
            title: 'Dashboard',
        });
    };

    const onGoToContacts = () => {
        navigation.navigate('ContactScreen', {
            title: 'Contacts',
        });
    };

    return (
        <View style={styles.body}>
            <View style={styles.contentBody}>
                <TouchableOpacity onPress={onGoToDashboard}>
                    <View style={styles.itemBody}>
                        <IconTachometer style={styles.iconTask} size={20} />
                        <Text style={styles.textBody}>Quản lý</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={onGoToContacts}>
                    <View style={styles.itemBody}>
                        <IconAddressBook style={styles.iconTask} size={25} />
                        <Text style={styles.textBody}>Thông báo</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default withNavigation(DrawerBody);
