import React, { useEffect } from 'react';
import { View, Text, SafeAreaView } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { getAllWallet } from '../../../redux/action/GetAllAction';

import DrawerHeader from './DrawerHeader';
import DrawerBody from './DrawerBody';
import DrawerFooter from './DrawerFooter';

function DrawerContent() {

    // const dispatch = useDispatch();
    // const token = useSelector((state) => state.AuthReducer.token)
    // console.log("🚀 ~ DrawerContent ~ token", token)

    // async function getWallet() {
    //     try {
    //         const response = await axios.get('http://localhost:3000/wallet/get',
    //             {
    //                 headers: {
    //                     'Authorization': `${token}`
    //                 }
    //             });
    //         if (response.data.status === 'success') {
    //             dispatch(getAllWallet(response.data.data))
    //         }
    //     } catch (error) {
    //         console.error('error =>', error);
    //     }
    // }


    // useEffect(() => {
    //     getWallet();
    // }, [])

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <DrawerHeader />
                <DrawerBody />
                <DrawerFooter />
            </View>
        </SafeAreaView>
    );
}

export default DrawerContent;