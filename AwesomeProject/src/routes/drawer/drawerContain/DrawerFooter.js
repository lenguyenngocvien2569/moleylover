import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import { IconCog, IconSignOut, IconQuestion } from '../../../assets/icons';
import { styles } from './styles';
import { useDispatch } from 'react-redux';
import { logout } from '../../../redux/action/AuthAction';

function DrawerFooter({ navigation }) {
    const dispatch = useDispatch();
    const onSettings = () => {
        navigation.navigate('SettingScreen', {
            title: 'Settings',
        });
    };

    const onLogout = () => {
        // const { setSignin } = global;
        // setSignin();
    };

    return (
        <View style={styles.footer}>
            <View style={styles.footer}>
                <View style={styles.contentFooter}>
                    <TouchableOpacity onPress={() => onSettings()}>
                        <View style={styles.itemFooter}>
                            <IconCog style={styles.iconFooter} />
                            <Text style={styles.textFooter}>Cài đặt</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={styles.itemFooter}>
                            <IconQuestion style={styles.iconFooter} />
                            <Text style={styles.textFooter}>helpdesk</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => dispatch(logout())}>
                        <View style={styles.itemFooter}>
                            <IconSignOut style={styles.iconLogout} />
                            <Text style={styles.textLogout}>Đăng xuất</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

export default withNavigation(DrawerFooter);
