import React from 'react';
import { View, Text, Image } from 'react-native';
import { styles } from './styles';
import { useSelector } from 'react-redux';
const images = {
    avatar: require('../../../assets/images/avatar.jpeg'),
};
function DrawerHeader() {
    const userInfo = useSelector((state) => state.AuthReducer.userInfo)

    return (
        <View style={styles.header}>
            <View style={styles.contentHeader}>
                {/* <Image styles={styles.avatar} source={{uri: getAvatar()}} /> */}
                <Image
                    style={styles.avatar}
                    source={images.avatar}
                />

                <Text style={styles.headerText}>{userInfo.username}</Text>

            </View>
        </View>
    );
}

export default DrawerHeader;