import React, { useEffect } from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';

//Routes
import BottomTab from '../tabbar/BottomTab';

//Components
import DrawerContent from './drawerContain';

import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { getAllWallet } from '../../redux/action/GetAllAction';

const Drawer = createDrawerNavigator();

export default DrawerNavigator = () => {

    // const dispatch = useDispatch();
    // const token = useSelector((state) => state.AuthReducer.token)
    // console.log("🚀 ~ token", token)

    // async function getWallet() {
    //     try {
    //         const response = await axios.get('http://localhost:3000/wallet/get',
    //             {
    //                 headers: {
    //                     'Authorization': `${token}`
    //                 }
    //             });
    //         if (response.data.status === 'success') {
    //             dispatch(getAllWallet(response.data.data))
    //         }
    //     } catch (error) {
    //         console.error('error =>', error);
    //     }
    // }


    // useEffect(() => {
    //     getWallet();
    // }, [])

    return (
        <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
            <Drawer.Screen name="BottomTab" component={BottomTab} />
        </Drawer.Navigator>
    );
};