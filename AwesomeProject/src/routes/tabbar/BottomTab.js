import React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
//Routes
import DashboardStack from '../dashboard/DashboardStack';
import NotificationStack from '../notification/NotificationStack';
import AddStack from '../add/addStack';

const Tab = createBottomTabNavigator();

export default DrawerNavigator = () => {

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Home') {
                        iconName = 'layer-group'
                    } else if (route.name === 'Service') {
                        iconName = 'box-open'
                    } else if (route.name === 'Add wallet') {
                        iconName = 'plus-circle'
                    }

                    // You can return any component that you like here!
                    return <FontAwesome5 name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: '#4ca750',
                inactiveTintColor: 'gray',
            }}
        >
            <Tab.Screen
                name="Home"
                component={DashboardStack}
            />
            <Tab.Screen
                name="Add wallet"
                component={AddStack}
            />
            <Tab.Screen
                name="Service"
                component={NotificationStack}
            />
        </Tab.Navigator>
    );
};