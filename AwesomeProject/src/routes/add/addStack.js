import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//Components
import AddScreen from '../../screens/add';

const Stack = createStackNavigator();

export default addStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="AddScreen" component={AddScreen} />
        </Stack.Navigator>
    );
};