import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//Components
import SplashScreen from '../../screens/root/SplashScreen';

//Routes
import AuthenticationStack from '../authentication/AuthenticationStack';
import DrawerNavigation from '../drawer/DrawerNavigation';
import { useSelector } from 'react-redux';


const Stack = createStackNavigator();

const fadeAnimation = ({ current }) => ({
    cardStyle: {
        opacity: current.progress,
    },
});

function RootNavigation(props) {
    const userInfo = useSelector((state) => state.AuthReducer.userInfo)
    const isSplash = useSelector((state) => state.AuthReducer.isSplash)

    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            {isSplash ? (
                <Stack.Screen name="SplashScreen" component={SplashScreen} />
            ) : userInfo.id ? (
                <Stack.Screen name="DrawerNavigation" component={DrawerNavigation} />
            ) : (
                <Stack.Screen name="Authentication" component={AuthenticationStack} />
            )}
        </Stack.Navigator>
    );
}

export default RootNavigation;