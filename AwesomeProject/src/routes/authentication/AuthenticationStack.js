import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { View, Text, Button } from 'react-native';
//Components
import LoginScreen from '../../screens/authentication/login';
import RegisterScreen from '../../screens/authentication/registerScreen';

const Stack = createStackNavigator();
export default AuthenticationStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="LoginScreen" component={LoginScreen} />
            <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        </Stack.Navigator>
    );
};