import { DATA_STATUS } from "../utils/config";
import {
  GetAllWalletERP,
} from "../services/ResWallet";

/* All POST */
const GetAllWalletBusiness = () => {
  try {
    return new Promise(async (res, rej) => {
      let allWallet = await GetAllWalletERP();
      console.log("🚀 ~ returnnewPromise ~ allWallet", allWallet)
      if (allWallet.status === DATA_STATUS.SUCCESS) {
        let customData = allWallet.data.data.payload;
        console.log("🚀 ~ returnnewPromise ~ customData", customData)
        res({
          data: customData,
          status: DATA_STATUS.SUCCESS,
        });
      } else {
        rej(allWallet);
      }
    });
  } catch (error) {
    error({
      data: [],
      status: DATA_STATUS.FAILED,
    });
  }
};

export {
  GetAllWalletBusiness,
};
